Router.route('/', {
    name: 'login',
    template: 'login'
});

Router.route('/home', {
    onBeforeAction: function () {
        if (Roles.userIsInRole(Meteor.userId(), "admin")) {
            this.next();
        }
        else {
            this.render('login');
        }
    },
    action: function () {
        this.render('home');
        this.layout('mainlayout');
    }
});

Router.route('/uploadimage', {
    action: function () {
        this.render('uploadimage');
    }
});

Router.route('/password_reset', {
    action: function () {
        this.render('password_reset');
        this.layout('mainlayout');
    }
});

Router.configure({
    // the default layout
    layoutTemplate: 'mainlayout'
});


var requireLogin = function () {
    if (!Meteor.user()) {
        this.render('login');
    }
    else {
        this.next();
    }
};

Router.onBeforeAction(requireLogin);
