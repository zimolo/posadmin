Template.login.events({
    'submit .login_form': function (event) {
        event.preventDefault();
        var username = event.target.username.value;
        var password = event.target.password.value;

        Meteor.loginWithPassword(username, password, function (err) {
            if (err) {
                alert(err);
            }
            else {
                Router.go('/home');
            }
        });
    }
});
