Template.password_reset.onCreated(function () {
    Meteor.call('check_tenant', function (err, res) {
        Session.set('tenant_list', res)
    });
});

Template.password_reset.helpers({
    'get_users': function () {
        // return Meteor.users.find();
        return Session.get('tenant_list');
    }
});

Template.password_reset.events({
    'click #submit_change_password': function () {
        // console.log($('[name=users]:selected').val())
        Meteor.call('reset_password', $('[name=users]:selected').val(), $('#new_login_password').val(), $('#new_validCode').val(), function (err, res) {
            if (res) {
                $('#new_login_password').val('');
                $('#new_validCode').val('');
                $("#select_user").val('请选择用户');
                alert('更改成功')
            }
        })
    }
})