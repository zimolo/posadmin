Template.uploadimage.onCreated(function(){
    Session.set('eatmeals',"");
    Meteor.call('check_tenant', function (err,res){
        Session.set('tenant_list',res)
    });
});

//Upload logo
Template.uploadimage.helpers({
    'logo': function () {
        return Session.get('logoUrl');
    },
    'image1': function () {
        return Session.get('image1Url');
    },
    'image2': function () {
        return Session.get('image2Url');
    },
    'eatmeals_able': function () {
        if(Session.get('eatmeals') == true) {
            return true;
        } else {
            return false;
        }
    }
})

Template.uploadimage.events({
    "change #file_upload_logo": function (e) {
        var files = $("input.file_bag")[0].files;

        S3.upload({
            files: files,
            path: Session.get('username') + "/logo"
        }, function (e, r) {
            Session.set('logoUrl', r.url);
        });
    },
    "change #file_upload_photo1": function (e) {
        var files = $("input.file_bag")[1].files;

        S3.upload({
            files: files,
            path: Session.get('username') + "/photo"
        }, function (e, r) {
            Session.set('image1Url', r.url);
        });
    },
    "change #file_upload_photo2": function (e) {
        var files = $("input.file_bag")[2].files;

        S3.upload({
            files: files,
            path: Session.get('username') + "/photo"
        }, function (e, r) {
            Session.set('image2Url', r.url);
        });
    }
})

Template.uploadimage.events({
    'change #signupAddress': function () {
        TimeZoned.getTimeZoneForAddress($('#signupAddress').val(), function (error, tz) {
            $('#timezone').val(tz)
        });
    },
    'change #eatmeals': function() {
        Session.set('eatmeals',$("#eatmeals").is(':checked'));
    },
    'change #select_user': function (res, err) {
        var username = res.target.value;
        console.log(username)
        var user = Meteor.users.findOne({username: username});
        var profile = UserProfile.findOne({user_id: user._id});
        if (profile) {
            $('#latitude').val(profile.latitude);
            $('#longitude').val(profile.longitude);
            $('#validCode').val(profile.valid_code);            
            $('#signupCnName').val(profile.chinese_name);
            $('#signupEnName').val(profile.english_name);
            $('#signupAddress').val(profile.address);
            $('#timezone').val(profile.timezone);
            $('#signupCity').val(profile.city);
            $('#signupProvince').val(profile.province);
            $('#signupPhone').val(profile.phone);
            $('#signupDesc').val(profile.description);
            Session.set('logoUrl',profile.shop_logo_url);
            Session.set('image1Url',profile.shop_image1_url);
            Session.set('image2Url',profile.shop_image2_url);
            console.log(profile.eatmeals);
            Session.set('eatmeals',profile.eatmeals);
            $('#eatmeals').prop('checked', Session.get('eatmeals'));
            $('#eatmeals_pay').prop('checked', profile.eatmeals_pay);
        }
        else {
            $('#latitude').val('');
            $('#longitude').val('');
            $('#timezone').val('');
            $('#validCode').val("");            
            $('#signupCnName').val("");
            $('#signupEnName').val("");
            $('#signupAddress').val("");
            $('#signupCity').val("");
            $('#signupProvince').val("");
            $('#signupPhone').val("");
            $('#signupDesc').val("");
            Session.set('logoUrl',null);
            Session.set('image1Url',null);
            Session.set('image2Url',null);
            Session.set('eatmeals',"");
            $('#eatmeals').prop('checked', false);
            $('#eatmeals_pay').prop('checked', true);


        }
        Session.set("selected_user", user)
    },
    'click #submit_profile': function () {
        var shop_uuid = uuid.new();
        var username = $("#select_user").val();
        var user_id = Meteor.users.findOne({username: username})._id;
        var validCode = $('#validCode').val();
        var chineseName = $('#signupCnName').val();
        var englishName = $('#signupEnName').val();
        var address = $('#signupAddress').val();
        var timezone = $('#timezone').val();
        var latitude = $('#latitude').val();
        var longitude = $('#longitude').val();
        var city = $('#signupCity').val();
        var province = $('#signupProvince').val();
        var phone = $('#signupPhone').val();
        var desc = $('#signupDesc').val();
        var eatmeals = $("#eatmeals").is(':checked');
        if(eatmeals == true){
            var eatmeals_pay = $("#eatmeals_pay").is(':checked');
        } else {
            var eatmeals_pay = "";
        }
        var shop_logo_url = Session.get('logoUrl');
        var shop_photo1_url = Session.get('image1Url');
        var shop_photo2_url = Session.get('image2Url');
        if (user_id == "" || user_id == undefined || username == "" || username == undefined || chineseName == "" || englishName == "" || address == "" || city == "" || province == "" || phone == "" || desc == "") {
            alert("请再次检查!");
        } else {
            Meteor.call('create_tenant_info', shop_uuid, user_id, username, validCode, chineseName, englishName, address, timezone, latitude, longitude, city, province, phone, desc, eatmeals, eatmeals_pay, shop_logo_url, shop_photo1_url, shop_photo2_url, function (err, res) {
                if (res) {
                    alert("上传成功");
                    $('#latitude').val('');
                    $('#longitude').val('');
                    $('#timezone').val('');
                    $('#validCode').val('');
                    $('#signupCnName').val('');
                    $('#signupEnName').val('');
                    $('#signupAddress').val('');
                    $('#signupCity').val('');
                    $('#signupProvince').val('');
                    $('#signupPhone').val('');
                    $('#signupDesc').val('');
                    $('#eatmeals').prop('checked', false);
                    Session.keys = {};
                    Router.go('/home');
                } else {
                    alert("上传失败");
                }
            });
        }

        console.log(user_id)
    }
});

Template.uploadimage.helpers({
    'get_users': function () {
        // return Meteor.users.find();
        return Session.get('tenant_list');
    }
});

