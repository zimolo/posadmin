Template.home.events({
    'submit .signup_form': function (event) {
        event.preventDefault();
        var username = $('#signupUsername').val();
        var tenant_group = $('#signupTenantGroup').val();
        var password = $('#signupPassword').val();
        var confirmPassword = $('#signupConfirmPassword').val();
        var validCode = $('#validCode').val();

        Session.set('username', username);

        if (password == confirmPassword) {
            Meteor.call('create_tenant', username, password, function (err) {
                if (!err) {
                    alert("Tenant注册完成，请上传图标");
                    // Go to next step adding images
                    Router.go('/uploadimage');
                }
                else {
                    alert(err);
                }
            });
        }
        else {
            alert("password doesn't match !");
        }

    }
});

Template.mainlayout.events({
    'click .logout': function (event) {
        event.preventDefault();
        Meteor.logout();
        Router.go('/');
    }
});
