//Adding S3 config file to gain access for upload
S3.config = {
    key: 'AKIAJ2KWHPZU244DGB3Q',
    secret: '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',
    bucket: 'zimolo',
    region: 'us-west-2' // Only needed if not "us-east-1" or "us-standard"
};

Meteor.startup(function () {
    console.log('Server started');
    // #Users and Permissions -> -> Creating the admin user
    if (Meteor.users.find().count() === 0) {
        console.log('Created Admin user');
        var userId = Accounts.createUser({
            username: 'superadmin',
            password: 'superadmin'
        });
        Roles.addUsersToRoles(userId, "admin");
    }

});

Meteor.methods({
    'check_tenant': function () {

        return Meteor.users.find({roles: ['tenant']}).fetch();
    },
    'create_tenant': function (username, password, tenant_group) {
        var userId = Accounts.createUser({
            username: username,
            password: password,
            tenant_name: tenant_group
        });
        let groupId = uuid.new();
        Roles.addUsersToRoles(userId, "tenant");
        // Meteor.users.update({_id:userId}, {$set:{tenant_id: groupId}});
    },
    'create_tenant_info': function (shop_uuid, user_id, username, validCode, chineseName, englishName, address, timezone, latitude, longitude, city, province, phone, desc, eatmeals, eatmeals_pay, shop_logo_url, shop_photo1_url, shop_photo2_url) {
        if (UserProfile.findOne({user_id: user_id})) {
            UserProfile.update({user_id: user_id}, {
                $set: {
                    valid_code: validCode,
                    chinese_name: chineseName,
                    english_name: englishName,
                    address: address,
                    timezone: timezone,
                    latitude: latitude,
                    longitude: longitude,
                    city: city,
                    province: province,
                    phone: phone,
                    description: desc,
                    eatmeals: eatmeals,
                    eatmeals_pay: eatmeals_pay,
                    shop_logo_url: shop_logo_url,
                    shop_image1_url: shop_photo1_url,
                    shop_image2_url: shop_photo2_url
                }
            });
        }
        else {
            UserProfile.insert({
                _id: shop_uuid,
                user_id: user_id,
                username: username,
                valid_code: validCode,
                chinese_name: chineseName,
                english_name: englishName,
                address: address,
                timezone: timezone,
                city: city,
                province: province,
                phone: phone,
                description: desc,
                eatmeals: eatmeals,
                eatmeals_pay: eatmeals_pay,
                shop_logo_url: shop_logo_url,
                shop_image1_url: shop_photo1_url,
                shop_image2_url: shop_photo2_url,
                require_password: false
            });
            Book.insert({
                "tenant_name": username,
                "tenant_id": user_id,
                "name": "today",
                "sales": 0,
                "cash": 0,
                "card": 0,
                "tips": 0
            });
            OrderSeq.insert({"tenant_name": username, "tenant_id": user_id, increment: parseInt(0)});
            ItemSeq.insert({"tenant_name": username, "tenant_id": user_id, increment: parseInt(0)});
            ReceiptSeq.insert({"tenant_name": username, "tenant_id": user_id, increment: parseInt(0)});
            TakeOutSeq.insert({"tenant_name": username, "tenant_id": user_id, increment: parseInt(0)});
        }
        return true;
    },
    'reset_password': function (username, login_pwd, office_pwd) {
        if (login_pwd !== "") {
            let user_id = Meteor.users.findOne({username:username})._id;
            Accounts.setPassword(user_id, login_pwd);
        }
        if(office_pwd !== "") {
            UserProfile.update({username:username},{$set:{valid_code:office_pwd}})
        }
        return true;
    }
});

