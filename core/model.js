UserProfile = new Mongo.Collection("user_profile");

Book = new Mongo.Collection('book');

OrderSeq = new Mongo.Collection("order_counter");

ItemSeq = new Mongo.Collection("item_counter");

ReceiptSeq = new Mongo.Collection("receipt_counter");

TakeOutSeq = new Mongo.Collection("takeout_counter");